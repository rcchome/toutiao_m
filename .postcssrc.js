module.exports = {
  plugins: {
    //   autoprefixer: {
    //     browsers: ['Android >= 4.0', 'iOS >= 8'],
    //   },
    //这个插件会编译所有css  把px转成rem rootValue就是转换的基准值,可以写成函数   
    'postcss-pxtorem': {
      // rootValue: 37.5,
      //结构出对象里的file,file是一个字符串, 根据不同的编译路径 设置不同的rem基准大小
      //vant375/ 10=37.5 设计图是750 / 10=75    
      rootValue({ file }) {
        return file.indexOf('vant') !== -1 ? 37.5 : 75
      },
      propList: ['*'],
    },
  },
};
