import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
// 引入Vant组件库 
import Vant from 'vant'
import 'vant/lib/index.css'
// 全局样式
import './styles/index.less'
//引入设置rem基准值 把屏幕分成10份 html的字体大小就为 屏幕宽度/10
import 'amfe-flexible'
//引入格式化时间的插件
import '../src/utils/dayjs'

// 挂载 Vant组件库
Vue.use(Vant);



Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
