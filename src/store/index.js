import Vue from 'vue'
import Vuex from 'vuex'
import { getItem, setItem } from '@/utils/storage.js'
Vue.use(Vuex)

const TOKEN = "TOKEN_User"
export default new Vuex.Store({
  state: {
    //获取本地存储 查看上面的数据
    user: getItem(TOKEN)
  },
  mutations: {
    setUserStorage(state, data) {
      state.user = data
      //设置到本地存储
      setItem(TOKEN, state.user)
    }
  },
  actions: {
  },
  modules: {
  }
})
