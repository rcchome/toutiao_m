/**
 * 
 * 设置本地存储
 */

/** 
 * 
 * 设置item
*/
export const setItem = (name, item) => {
    if (typeof item === 'object') {
        item = JSON.stringify(item)
    }
    window.localStorage.setItem(name, item)
}

/**
 * 
 * 获取item
 */
export const getItem = (name) => {
    try {
        return JSON.parse(window.localStorage.getItem(name))
    } catch (err) {
        return window.localStorage.getItem(name)
    }
}

/**
 *
 * 删除item
 */
export const removeItem = (name) => {
    window.localStorage.removeItem(name)
}