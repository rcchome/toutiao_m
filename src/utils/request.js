/**
 * 请求模块
 */
import axios from 'axios'
// 引入store
import store from '@/store/index.js'
// axiso.craete 创建axios 的实例对象
const request = axios.create({
    baseURL: 'http://ttapi.research.itcast.cn/'
})
// 添加请求拦截器
request.interceptors.request.use(function (config) {
    // 在发送请求之前做些什么
    const { user } = store.state
    if (user && user.token) {
        config.headers.Authorization = `Bearer ${store.state.user.token}`
    }
    return config
}, function (error) {
    // 对请求错误做些什么
    return Promise.reject(error)
});

export default request