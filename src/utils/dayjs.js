import Vue from 'vue'
import dayjs from 'dayjs' // ES 2015
import 'dayjs/locale/zh-cn' // 加载语言
//加载相对时间的插件
import relativeTime from 'dayjs/plugin/relativeTime'
dayjs.extend(relativeTime) //挂载插件

dayjs.locale('zh-cn') // 全局使用

// console.log(dayjs().to(dayjs('20200901')));
Vue.filter('FormatDate', val => {
    return dayjs().to(dayjs(val))
})