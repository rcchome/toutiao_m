
/**
 * 
 * 登用户相关的请求
 */
import request from '@/utils/request.js'
//登录
export const login = (data) => {
    return request.post('/app/v1_0/authorizations', data)
}
/** 
 * 
 * 发送验证码
*/
export const sendSms = (mobile) => {
    return request({
        method: 'GET',
        url: `/app/v1_0/sms/codes/${mobile}`,
    })
}
/** 
 * 
 * 获取用户信息
*/
export const getUserInfo = () => {
    return request({
        method: 'GET',
        url: `/app/v1_0/user`
    })
}

/** 
 * 
 * 获取用户频道列表
*/
export const userChannels = () => {
    return request({
        method: 'GET',
        url: `/app/v1_0/user/channels`
    })
}

