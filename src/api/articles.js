import request from '@/utils/request.js'
/**
 * 
 * 获取频道新闻推荐
*/
export const getArticles = params => {
    return request({
        method: 'GET',
        url: `/app/v1_1/articles`,
        params
    })
}